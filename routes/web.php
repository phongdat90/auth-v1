<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin', 'middleware' => 'isAdmin'], function() {
    //Route::get('/home', 'AdminHomeController@index');
});


Route::get('/admin/register', 'Admin\Auth\RegisterController@showRegistrationForm');
Route::post('/admin/register', 'Admin\Auth\RegisterController@register')->name('admin.register');
Route::get('/admin/login', 'Admin\Auth\LoginController@showLoginForm');
Route::post('/admin/login', 'Admin\Auth\LoginController@login')->name('admin.login');
Route::post('/admin/logout', 'Admin\Auth\LoginController@logout');

Route::group(['prefix' => 'admin', 'middleware' => 'Assign.guard:admin,admin/login'], function() {
    Route::get('/home', 'AdminHomeController@index');
});